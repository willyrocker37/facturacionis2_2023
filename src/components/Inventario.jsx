import React, {useState, useEffect} from 'react'

export const Inventario = () => {

    const obtenerRegistrosProductos = ()=>{
        var datos = localStorage.getItem("registrosproductos");
        if (datos !== null){
            return JSON.parse(datos);
        }else{
            return [];
        }
    }

    const [registrosproductos, setRegistrosProductos] = useState(obtenerRegistrosProductos());
    const [edicionIndex, setEdicionIndex] = useState(null);
 
  const actualizarProducto = (index, campo, valor) => {
    const nuevosRegistros = [...registrosproductos];
    nuevosRegistros[index][campo] = valor;
    setRegistrosProductos(nuevosRegistros);
  };
 
  const iniciarEdicion = (index) => {
    setEdicionIndex(index);
  };
 
  const guardarEdicion = () => {
    setEdicionIndex(null);
    localStorage.setItem('registrosproductos', JSON.stringify(registrosproductos));
  };

useEffect(()=>{
  localStorage.setItem("registrosproductos", JSON.stringify(registrosproductos))
},[registrosproductos])


  return (
    <div className="bg-light" style={{marginTop:20, padding:20}}>

    <div className="h3">
      Existencia de Productos
    </div>

    <div className="table-responsive">
      
      {registrosproductos.length > 0 ?

      <>
        <table className="table table-bordered table-hover" style={{marginTop:12}}>
            <thead className="text-center" style={{background:"lightgray"}}>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Marca</th>
                    <th>Tipo</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th>Actualizar</th>
                </tr>
            </thead>
            <tbody className="text-center align-baseline">
                {registrosproductos.map((x, index) => (
                  <tr key={index}>
                    <th>{index + 1}</th>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.nombre}
                          onChange={(e) => actualizarProducto(index, 'nombre', e.target.value)}
                        />
                      ) : (
                        x.nombre
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.marca}
                          onChange={(e) => actualizarProducto(index, 'marca', e.target.value)}
                        />
                      ) : (
                        x.marca
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.tipo}
                          onChange={(e) => actualizarProducto(index, 'tipo', e.target.value)}
                        />
                      ) : (
                        x.tipo
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.cantidad}
                          onChange={(e) => actualizarProducto(index, 'cantidad', e.target.value)}
                        />
                      ) : (
                        x.cantidad
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.precio}
                          onChange={(e) => actualizarProducto(index, 'precio', e.target.value)}
                        />
                      ) : (
                        x.precio
                      )}
                    </td>
                    <td className="text-center">
                      {edicionIndex === index ? (
                        <button className="btn btn-outline-success" onClick={guardarEdicion}>
                          Guardar
                        </button>
                      ) : (
                        <button className="btn btn-outline-primary" onClick={() => iniciarEdicion(index)}>
                          Editar
                        </button>
                      )}
                    </td>
                    <td>
                      <div style={{ marginTop: 20 }}>
 
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
        </table>
      </> 
      
      : <p className='h5' style={{color:"red"}}>"No hay registros para inventario</p>
       }
    </div>
 
  </div>
  )
}

export default Inventario