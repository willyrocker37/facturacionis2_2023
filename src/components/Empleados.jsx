import React, { useState, useEffect } from 'react';


export const Empleados = () => {

    const obtenerRegistrosEmp = () => {
        var datos = localStorage.getItem('registrosloginemp');
        if (datos !== null){
            return JSON.parse(datos);
        } else {
            return [];
        }
    }

    const [registrosloginemp, setRegistrosLoginEmp] = useState(obtenerRegistrosEmp());
    const [rol, setRol] = useState('');
    const [cedula, setCedula] = useState('');
    const [nombres, setNombres] = useState('');
    const [apellidos, setApellidos] = useState('');
    const [direccion, setDireccion] = useState('');
    const [telefono, setTelefono] = useState('');
    const [email, setEmail] = useState('');
    const [tipoEstado, setTipoEstado] = useState(""); //Activo-Inactivo


    const opcionesEstado = ['Activo', 'Inactivo'];
    const opcionesRol = ['Gerente', 'Supervisor', 'Empleado'];

    const botonGuardar = (e) => {
        e.preventDefault();
        var miObjeto = { rol, cedula, nombres, apellidos, direccion, telefono, email, tipoEstado };
        setRegistrosLoginEmp([...registrosloginemp, miObjeto]);
        limpiarFormulario();
    }

    useEffect(() => {
        localStorage.setItem('registrosloginemp', JSON.stringify(registrosloginemp));
    }, [registrosloginemp]);

    const limpiarFormulario = () => {
        setRol('');
        setCedula('');
        setNombres('');
        setApellidos('');
        setDireccion('');
        setTelefono('');
        setEmail('');
        setTipoEstado('');
        document.getElementById('miFormulario').reset();
    }

    return (
        <div className="bg-light" style={{ marginTop: 20, padding: 20 }}>
            <div className="h3">
                Formulario De Registro De Empleados
                <br />
                <form id="miFormulario" onSubmit={botonGuardar}>
                    <div className="row" style={{ marginTop: 20 }}>
                        <div className="col-12">
                            <h4>Datos del Empleado</h4>
                        </div>
                        <section>
                            <div className="col-6">
                                <select className='form-control form control-lg text-center' onChange={(e) => setRol(e.target.value)} required>
                                    <option value="" disabled selected>Seleccione un rol</option>
                                    {opcionesRol.map((opcion, index) => (
                                        <option key={index} value={opcion}>{opcion}</option>
                                    ))}
                                </select>
                                <input className='form-control form control-lg text-center' type='text' name="Cédula" id="Cédula" placeholder="Ingrese su cédula" onChange={(e)=>setCedula(e.target.value)}required/>
                            <input className='form-control form control-lg text-center' type='text' name="Nombres" id="Nombrs" placeholder="Ingrese sus Nombres" onChange={(e)=>setNombres(e.target.value)}required/>
                            <input className='form-control form control-lg text-center' type='text' name="Apellidos" id="Apellidos" placeholder="Ingrese sus Apellidos" onChange={(e)=>setApellidos(e.target.value)}required/>
                            <input className='form-control form control-lg text-center' type='text' name="Dirección" id="Dirección" placeholder="Ingrese su Dirección" onChange={(e)=>setDireccion(e.target.value)}required/>
                            <input className='form-control form control-lg text-center' type='text' name="Teléfono" id="Teléfono" placeholder="Ingrese su Teléfono" onChange={(e)=>setTelefono(e.target.value)}required/>
                            <input className='form-control form control-lg text-center' type='email' name="Email" id="Email" placeholder="Ingrese su Email" onChange={(e)=>setEmail(e.target.value)}required/>
                            <select
                                    className='form-control form control-lg text-center'
                                    onChange={(e) => setTipoEstado(e.target.value)}
                                    required
                                >
                                    <option value="" disabled selected>Seleccione un estado</option>
                                    {opcionesEstado.map((opcion, index) => (
                                        <option key={index} value={opcion}>{opcion}</option>
                                    ))}
                                </select>
                            </div>
                        </section>
                        <div className="row" style={{ marginTop: 20 }}>
                            <div className="col">
                                <button className="btn btn-primary btn-lg">Guardar Datos</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Empleados;