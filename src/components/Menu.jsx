import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { Empleados } from "./Empleados"
import { ListarEmpleados } from "./ListarEmpleados"
import { Clientes } from "./Clientes"
import { Listar } from "./Listar"
import { Facturar } from "./Facturar"
import { Productos } from "./Productos"
import { Inventario } from "./Inventario"
import {Ventas} from "./Ventas"

export const Menu = (props) => {

  const [user] = useState(localStorage.getItem("user"));
  const [emp, setEmp] = useState("");
  const [lie, setLie] = useState("");
  const [reg, setReg] = useState("");
  const [lis, setLis] = useState("");
  const [prod, setProd] = useState("");
  const [inv, setInv] = useState("");
  const [ven, setVen] = useState("");
  const [fac, setFac] = useState("");
  


  function cerrarSesion() {
    localStorage.removeItem("user");
    localStorage.removeItem("miLogin");
    document.getElementById("caja_menu").style.display = "none";
    document.getElementById("form_login").style.display = "block";
    document.getElementById("txtuser").value = "";
    document.getElementById("txtpass").value = "";
    document.getElementById("txtuser").focus();

  }

  function op_registrar() {
    setReg("1");
    setLis("0");
    setFac("0");
    setProd("0");
    setInv("0");
    setEmp("0");
    setLie("0");
  }
  function op_listar() {
    setReg("0");
    setLis("1");
    setFac("0");
    setProd("0");
    setInv("0");
    setEmp("0");
    setLie("0");
  }
  function op_facturar() {
    setReg("0");
    setLis("0");
    setFac("1");
    setProd("0");
    setInv("0");
    setEmp("0");
    setLie("0");
  }
  function op_productos() {
    setReg("0");
    setLis("0");
    setFac("0");
    setProd("1");
    setInv("0");
    setEmp("0");
    setLie("0");
  }
  function op_inventario() {
    setReg("0");
    setLis("0");
    setFac("0");
    setProd("0");
    setInv("1");
    setEmp("0");
    setLie("0");
  }

  function op_Empleados() {
    setReg("0");
    setLis("0");
    setFac("0");
    setProd("0");
    setInv("0");
    setEmp("1");
    setLie("0");
  }
  function op_listarEmpleados() {
    setReg("0");
    setLis("0");
    setFac("0");
    setProd("0");
    setInv("0");
    setEmp("0");
    setLie("1");
  }

  function op_Ventas() {
    setReg("0");
    setLis("0");
    setFac("0");
    setProd("0");
    setInv("0");
    setEmp("0");
    setLie("0");
    setVen("1");
  }
  return (
    <>

      <div id="caja_menu" style={{ textAlign: "left" }}>

        <strong className="h3">
          Bienvenido Usuario : {user.toUpperCase()}
        </strong>

        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{ marginTop: 20 }}>
          <div className="container-fluid">

            <label className="navbar-brand  h5" href=" ">Menú Principal</label>

            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_Empleados}>Empleados</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_listarEmpleados}>Listar Empleados</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_registrar}>Clientes</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_listar}>Reporte Clientes</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_productos}>Productos</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_inventario}>Inventario</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_Ventas}>Ventas</NavLink>
                <NavLink to="" className="nav-link  h5  text-center" onClick={op_facturar}>Facturar</NavLink>
                <a className="nav-link  h5  text-center" style={{ color: "blue" }} href=" " onClick={cerrarSesion} >Cerrar Sesión</a>
              </div>
            </div>
          </div>
        </nav>
      </div>
      {emp === "1" && <Empleados />}
      {lie === "1" && <ListarEmpleados />}
      {reg === "1" && <Clientes />}
      {lis === "1" && <Listar />}
      {prod === "1" && <Productos />}
      {inv === "1" && <Inventario />}
      {ven === "1" && <Ventas />}
      {fac === "1" && <Facturar />}
      

    </>
  )
}

export default Menu