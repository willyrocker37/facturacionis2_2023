import React, { useState, useEffect } from 'react';

export const ListarEmpleados = () => {
  const obtenerRegistrosEmp = () => {
    var datos = localStorage.getItem('registrosloginemp');
    if (datos !== null) {
      return JSON.parse(datos);
    } else {
      return [];
    }
  };

  const [registrosloginemp, setRegistrosLoginEmp] = useState(obtenerRegistrosEmp());
  const [edicionIndex, setEdicionIndex] = useState(null);

  const actualizarEmpleado = (index, campo, valor) => {
    const nuevosRegistros = [...registrosloginemp];
    nuevosRegistros[index][campo] = valor;
    setRegistrosLoginEmp(nuevosRegistros);
  };
  const iniciarEdicion = (index) => {
    setEdicionIndex(index);
  };
  
  const guardarEdicion = () => {
    setEdicionIndex(null);
    localStorage.setItem('registrosloginemp', JSON.stringify(registrosloginemp));
  };
 

  useEffect(() => {
    localStorage.setItem('registrosloginemp', JSON.stringify(registrosloginemp));
  }, [registrosloginemp]);

  return (
    <div className="bg-light" style={{ marginTop: 20, padding: 20 }}>
      <div className="h3">Listado De Empleados</div>
      <div className="table-responsive">
        {registrosloginemp.length > 0 ? (
          <>
            <table className="table table-bordered table-hover" style={{ marginTop: 12 }}>
              <thead className="text-center" style={{ background: 'lightgray' }}>
              <tr>
                  <th>#</th>
                  <th>Rol</th>
                  <th>Cédula</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Estado</th>
                  <th>Actualizar</th>
                </tr>
              </thead>
              <tbody className="text-center align-baseline">
                {registrosloginemp.map((x, index) => (
                  <tr key={index}>
                    <th>{index + 1}</th>
                    <td>
                    {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.rol}
                          onChange={(e) => actualizarEmpleado(index, 'rol', e.target.value)}
                        />
                      ) : (
                        x.rol
                      )}
                    </td>
                    <td>
                    {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.cedula}
                          onChange={(e) => actualizarEmpleado(index, 'cedula', e.target.value)}
                        />
                      ) : (
                        x.cedula
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.nombres}
                          onChange={(e) => actualizarEmpleado(index, 'nombres', e.target.value)}
                        />
                      ) : (
                        x.nombres
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.apellidos}
                          onChange={(e) => actualizarEmpleado(index, 'apellidos', e.target.value)}
                        />
                      ) : (
                        x.apellidos
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.direccion}
                          onChange={(e) => actualizarEmpleado(index, 'direccion', e.target.value)}
                        />
                      ) : (
                        x.direccion
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.telefono}
                          onChange={(e) => actualizarEmpleado(index, 'telefono', e.target.value)}
                        />
                      ) : (
                        x.telefono
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.email}
                          onChange={(e) => actualizarEmpleado(index, 'email', e.target.value)}
                        />
                      ) : (
                        x.email
                      )}
                        </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.tipoEstado}
                          onChange={(e) => actualizarEmpleado(index, 'tipoEstado', e.target.value)}
                        />
                      ) : (
                        x.tipoEstado
                      )}
                    </td>
                    <td className="text-center">
                      {edicionIndex === index ? (
                        <button className="btn btn-outline-success" onClick={guardarEdicion}>
                          Guardar
                        </button>
                      ) : (
                        <button className="btn btn-outline-primary" onClick={() => iniciarEdicion(index)}>
                          Editar
                        </button>
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </>
        ) : (
          <p className="h5" style={{ color: 'red' }}>
            No hay registros para listar
          </p>
        )}
      </div>
    </div>
  );
};

export default ListarEmpleados;