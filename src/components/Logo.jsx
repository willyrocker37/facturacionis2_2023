import React from 'react'

export const Logo = () => {
  return (
    <div style={{float: "left"}}> 
      <img src="logos.png" alt="Logo de la tienda"/>
    </div>
  )
}

export default Logo
