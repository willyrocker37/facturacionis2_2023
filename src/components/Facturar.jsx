import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button, Row, Col, Container } from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import Form from 'react-bootstrap/Form';


export const Facturar = () => {
  const obtenerRegistrosProductos = () => {
    var datos = localStorage.getItem('registrosproductos');
    if (datos !== null) {
      return JSON.parse(datos);
    } else {
      return [];
    }
  };

  const obtenerRegistrosClientes = () => {
    var datos = localStorage.getItem('registroslogin');
    if (datos !== null) {
      return JSON.parse(datos);
    } else {
      return [];
    }
  };
  const [clienteSeleccionado, setClienteSeleccionado] = useState(null);

  const [clienteSeleccionadoBusqueda, setClienteSeleccionadoBusqueda] = useState(null);

  const seleccionarCliente = (cliente) => {
    setClienteSeleccionado(cliente);
    setClienteSeleccionadoBusqueda(cliente);
  };
  const [registrosclientes, setRegistrosClientes] = useState(obtenerRegistrosClientes());
  const [busqueda, setBusqueda] = useState('');
  const [resultadosBusqueda, setResultadosBusqueda] = useState([]);

  const handleBusqueda = () => {
    const resultados = registrosclientes.filter((cliente) =>
      cliente.cedula.toLowerCase().includes(busqueda.toLowerCase()) ||
      cliente.nombres.toLowerCase().includes(busqueda.toLowerCase()) ||
      cliente.apellidos.toLowerCase().includes(busqueda.toLowerCase())
    );
    setResultadosBusqueda(resultados);
  };

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
        href=""
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        {children}
        &#x25bc;
    </a>
));




  const [registrosproductos, setRegistrosProductos] = useState(obtenerRegistrosProductos());
  const [productosSeleccionados, setProductosSeleccionados] = useState([]);


  const agregarProducto = (producto) => {
    setProductosSeleccionados([...productosSeleccionados, producto]);
  };

  const eliminarProducto = (index) => {
    const nuevosProductos = [...productosSeleccionados];
    nuevosProductos.splice(index, 1);
    setProductosSeleccionados(nuevosProductos);
  };
  const [showModalProducto, setShowModalProducto] = useState(false);
  const [productoSeleccionado, setProductoSeleccionado] = useState(null);
  const [cantidadProducto, setCantidadProducto] = useState(1);

  const abrirModalProducto = (producto) => {
    setProductoSeleccionado(producto);
    setShowModalProducto(true);
  };


  const calcularTotalSinIVA = () => {
    return productosSeleccionados.reduce((total, miProducto) => total + (miProducto.precio * miProducto.cantidad), 0);
  };

  const totalFactura = () => {
    const totalSinIVA = calcularTotalSinIVA();
    const totalConIVA = totalSinIVA * 1.12; // Sumar el IVA del 12%
    return {
      totalSinIVA: totalSinIVA.toFixed(2),
      totalConIVA: totalConIVA.toFixed(2) // Redondear a dos decimales
    };
  };



  const [tipoPago, setTipoPago] = useState('efectivo'); // 'efectivo' o 'credito'
  const cambiarTipoPago = (tipo) => {
    setTipoPago(tipo);
  };


  const confirmarFactura = () => {
    // Crear ventana emergente
    const ventanaConfirmacion = document.createElement('div');
    ventanaConfirmacion.className = 'ventana-confirmacion'; // Estilo de la ventana de confirmación

    // Contenido de la ventana emergente
    ventanaConfirmacion.innerHTML = `
        <h2>Confirmar factura</h2>
        <p>¿Estás seguro de que quieres confirmar esta factura?</p>
        <button onclick="procesarFactura()">Confirmar</button>
        <button onclick="cancelarFactura()">Cancelar</button>
      `;

    // Agregar la ventana emergente al cuerpo del documento
    document.body.appendChild(ventanaConfirmacion);
  };

  const procesarFactura = () => {
    // Generar un número de factura con el formato XXX-XXX-XXXXXXXXX
    const numeroAleatorio = Math.floor(Math.random() * 10000000000).toString().padStart(10, '0');
    const numeroFormato = `${numeroAleatorio.substr(0, 3)}-${numeroAleatorio.substr(3, 3)}-${numeroAleatorio.substr(6)}`;

    // Actualizar el estado con el número de factura generado
    setNumeroFactura(numeroFormato);

    // Resto de la lógica para confirmar la factura y utilizar el número de factura generado
    // ...

    console.log("Factura procesada. Número de factura:", numeroFormato);

    // Cerrar la ventana emergente
    const ventanaConfirmacion = document.querySelector('.ventana-confirmacion');
    ventanaConfirmacion.parentNode.removeChild(ventanaConfirmacion);
  };


  const cancelarFactura = () => {
    // Cerrar la ventana emergente
    const ventanaConfirmacion = document.querySelector('.ventana-confirmacion');
    ventanaConfirmacion.parentNode.removeChild(ventanaConfirmacion);
  };
  const [facturaGuardada, setFacturaGuardada] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);

  // Crear una función para guardar la factura
  const guardarFactura = () => {
    // Recopilar todos los datos seleccionados
    const factura = {
      cliente: clienteSeleccionado,
      productos: productosSeleccionados,
      tipoPago: tipoPago,
      numeroFactura: numeroFactura,
      fecha: fechaActual,
      totalSinIVA: calcularTotalSinIVA(),
      totalConIVA: totalFactura().totalConIVA
      // Otros datos relevantes de la factura
    };

    setFacturaGuardada(factura); // Guardar la factura en el estado local

    // Mostrar la representación visual de la factura en un modal
    handleShowModal();
  };


  // Agregar un estado para la cantidad seleccionada
  const [cantidadSeleccionada, setCantidadSeleccionada] = useState(1); // Por defecto, se selecciona 1 producto.

  // Función para manejar el cambio en la cantidad seleccionada
  const handleCantidadChange = (e) => {
    setCantidadSeleccionada(parseInt(e.target.value));
  };

  // Modificar la función agregarProducto para incluir la cantidad seleccionada
  const agregarProductoConCantidad = (producto) => {
    const productoConCantidad = {
      ...producto,
      cantidad: cantidadSeleccionada
    };
    setProductosSeleccionados([...productosSeleccionados, productoConCantidad]);
  };

  const [numeroFactura, setNumeroFactura] = useState('');

  useEffect(() => {
    obtenerFechaActual(); // Actualizar la fecha actual al montar el componente
    generarNumeroFactura(); // Generar número de factura único
  }, []);

  // Función para obtener la fecha actual
  const [fechaActual, setFechaActual] = useState('');

  // Función para obtener la fecha actual
  const obtenerFechaActual = () => {
    const fecha = new Date();
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const fechaFormateada = fecha.toLocaleDateString('es-ES', options);
    setFechaActual(fechaFormateada);
  };

  useEffect(() => {
    obtenerFechaActual(); // Actualizar la fecha actual al montar el componente
  }, []);


  // Función para generar un número de factura único
  const generarNumeroFactura = () => {
    // Obtener los primeros tres dígitos del RUC como número de establecimiento (simulado)
    const establecimiento = '002'; // Cambiar según tu lógica real

    // Obtener los siguientes tres dígitos como número de facturero (simulado)
    const facturero = '001'; // Cambiar según tu lógica real

    // Generar un número aleatorio de 9 dígitos como número de factura
    const numeroAleatorio = Math.floor(Math.random() * 1000000000).toString().padStart(9, '0');

    // Formatear el número de factura según el formato XXX-XXX-XXXXXXXXX
    const numeroFormato = `${establecimiento}-${facturero}-${numeroAleatorio}`;

    // Actualizar el estado con el número de factura generado
    setNumeroFactura(numeroFormato);
  };




  return (
    <div className="bg-light" style={{ marginTop: 20, padding: 20, minHeight: '500px' }}>
      <div className="h3">Facturación</div>
      <div className="h5">Fecha actual: {fechaActual}</div>
      <div className="h5">Número de factura: {numeroFactura}</div>
      <div style={{ height: '20px' }}></div>
      <div className="table-responsive">
        <div>
          {/* Agregar campo de búsqueda */}
          <Dropdown>
            <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
              Buscar cliente...
            </Dropdown.Toggle>
            <Dropdown.Menu style={{ maxHeight: '300px', overflowY: 'auto' }}>
              <Form.Control
                type="text"
                placeholder="Buscar cliente..."
                value={busqueda}
                onChange={(e) => setBusqueda(e.target.value)}
                />
              <Button onClick={handleBusqueda}>Buscar</Button>
            </Dropdown.Menu>
          </Dropdown>
          {/* Mostrar resultados de la búsqueda */}
          {resultadosBusqueda.length > 0 && (
            <div>
              <h4>Resultados de la búsqueda:</h4>
              <ul>
                {resultadosBusqueda.map((cliente, index) => (
                  <li key={index}>
                    {cliente.nombres} {cliente.apellidos}{' '}
                    <button className="btn btn-outline-success" onClick={() => seleccionarCliente(cliente)}>
                      Seleccionar
                    </button>
                  </li>
                ))}
              </ul>
            </div>
          )}
            </div>
            <div style={{ height: '100px' }}></div>



        <Modal show={showModalProducto} onHide={() => setShowModalProducto(false)}>
          <Modal.Header closeButton>
            <Modal.Title>{productoSeleccionado && productoSeleccionado.nombre}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="cantidadProducto">
                <Form.Label>Cantidad:</Form.Label>
                <Form.Control
                  type="number"
                  value={cantidadProducto}
                  onChange={(e) => setCantidadProducto(e.target.value)}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowModalProducto(false)}>
              Cancelar
            </Button>
            <Button variant="primary" onClick={() => agregarProductoConCantidad(productoSeleccionado)}>
              Agregar a la factura
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
      {registrosproductos.length > 0 ? (
        <div>
          <div className="h4" style={{ marginTop: 20 }}>
            Productos Seleccionados
          </div>
          <table className="table table-bordered table-hover" style={{ marginTop: 12 }}>
            <thead className="text-center" style={{ background: 'lightgray' }}>
              <tr>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Tipo</th>
                <th>Cantidad</th>
                <th>Subtotal</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody className="text-center align-baseline">
              {registrosproductos.map((producto, index) => (
                <tr key={index}>
                  <td>{producto.nombre}</td>
                  <td>{producto.marca}</td>
                  <td>{producto.tipo}</td>
                  <td>{producto.cantidad}</td>
                  <td>{producto.precio * producto.cantidad}</td>
                  <td>
                    <button className="btn btn-outline-primary" onClick={() => abrirModalProducto(producto)}>
                      Seleccionar
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ) : (
        <p className="h5" style={{ color: 'red' }}>
          No hay productos disponibles
        </p>
      )}
      {/* Sección para seleccionar tipo de pago */}
      <div style={{ marginTop: 20 }}>
        <h4>Seleccione tipo de pago:</h4>
        <div>
          <input
            type="radio"
            id="efectivo"
            name="tipoPago"
            value="efectivo"
            checked={tipoPago === 'efectivo'}
            onChange={() => cambiarTipoPago('efectivo')}
          />
          <label htmlFor="efectivo">Efectivo</label>
        </div>
        <div>
          <input
            type="radio"
            id="credito"
            name="tipoPago"
            value="credito"
            checked={tipoPago === 'credito'}
            onChange={() => cambiarTipoPago('credito')}
          />
          <label htmlFor="credito">Crédito</label>
        </div>
      </div>
      <div className="h4" style={{ marginTop: 20 }}>
        Total sin IVA: ${calcularTotalSinIVA()}
      </div>
      <div className="h4" style={{ marginTop: 20 }}>
        Total a Pagar (IVA incluido): ${totalFactura().totalConIVA}
      </div>
      <div style={{ marginTop: 20 }}>
        <Button variant="primary" onClick={guardarFactura}>
          Guardar Factura
        </Button>
      </div>
      <Modal show={showModal} onHide={handleCloseModal} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Factura Guardada</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>La factura ha sido guardada exitosamente.</p>
          <hr />
          {facturaGuardada && (
            <div>
              <Row>
                <Col>
                  <h1>Grupo 3</h1>
                  <p>Universidad Técnica del Norte</p>
                  <a href="http://localhost:3000/">http://localhost:3000/</a>
                  <hr></hr>
                  <h5>Factura dirigida a:</h5>
                  <p>Cédula: {facturaGuardada.cliente.cedula}</p>
                  <p>Nombre: {facturaGuardada.cliente.nombres}</p>
                  <p>Apellidos: {facturaGuardada.cliente.apellidos}</p>
                  <p>Dirección: {facturaGuardada.cliente.direccion}</p>
                  <p>Teléfono: {facturaGuardada.cliente.telefono}</p>
                  <p>Email: {facturaGuardada.cliente.email}</p>
                </Col>
                <Col>
                  <h1>FACTURA</h1>
                  <p>Enlace para añadir un logo</p>
                  <p>Proyecto de facturación</p>
                  <hr></hr>
                  <h5>Datos de la factura:</h5>
                  <p>Número de Factura: {numeroFactura}</p>
                  <p>Fecha: {fechaActual}</p>
                </Col>
              </Row>
              <table className="table">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Marca</th>
                    <th>Tipo</th>
                    <th>Cantidad</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  {facturaGuardada.productos.map((miProducto, index) => (
                    <tr key={index}>
                      <td>{miProducto.nombre}</td>
                      <td>{miProducto.marca}</td>
                      <td>{miProducto.tipo}</td>
                      <td>{miProducto.cantidad}</td>
                      <td>{miProducto.precio * miProducto.cantidad}</td>
                    </tr>
                  ))}
                </tbody>
                <tfoot>
                  <tr>
                    <td colSpan="4" className="text-end">
                      Total sin IVA:
                    </td>
                    <td>${calcularTotalSinIVA()}</td>
                  </tr>
                  <tr>
                    <td colSpan="4" className="text-end">
                      Total a Pagar (IVA incluido):
                    </td>
                    <td>${totalFactura().totalConIVA}</td>
                  </tr>
                  <tr>
                    <td colSpan="4" className="text-end">
                      Tipo de pago:
                    </td>
                    <td>{tipoPago}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Cerrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Facturar;