import React, { useState, useEffect } from 'react';

export const Listar = () => {
  const obtenerRegistros = () => {
    var datos = localStorage.getItem('registroslogin');
    if (datos !== "null") {
      return JSON.parse(datos);
    } else {
      return [];
    }
  };
 
  const [registroslogin, setRegistrosLogin] = useState(obtenerRegistros());
  const [edicionIndex, setEdicionIndex] = useState(null);
 
  const actualizarCliente = (index, campo, valor) => {
    const nuevosRegistros = [...registroslogin];
    nuevosRegistros[index][campo] = valor;
    setRegistrosLogin(nuevosRegistros);
  };
 
  const iniciarEdicion = (index) => {
    setEdicionIndex(index);
  };
 
  const guardarEdicion = () => {
    setEdicionIndex(null);
    localStorage.setItem('registroslogin', JSON.stringify(registroslogin));
  };
 
  useEffect(() => {
    localStorage.setItem('registroslogin', JSON.stringify(registroslogin));
  }, [registroslogin]);
 
  return (
    <div className="bg-light" style={{ marginTop: 20, padding: 20 }}>
      <div className="h3">Listado De Clientes</div>
      <div className="table-responsive">
        {registroslogin.length > 0 ? (
          <>
            <table className="table table-bordered table-hover" style={{ marginTop: 12 }}>
              <thead className="text-center" style={{ background: 'lightgray' }}>
                <tr>
                  <th>#</th>
                  <th>Cédula</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Email</th>
                  <th>Estado</th>
                  <th>Actualizar</th>
 
                </tr>
              </thead>
              <tbody className="text-center align-baseline">
                {registroslogin.map((x, index) => (
                  <tr key={index}>
                    <th>{index + 1}</th>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.cedula}
                          onChange={(e) => actualizarCliente(index, 'cedula', e.target.value)}
                        />
                      ) : (
                        x.cedula
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.nombres}
                          onChange={(e) => actualizarCliente(index, 'nombres', e.target.value)}
                        />
                      ) : (
                        x.nombres
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.apellidos}
                          onChange={(e) => actualizarCliente(index, 'apellidos', e.target.value)}
                        />
                      ) : (
                        x.apellidos
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.direccion}
                          onChange={(e) => actualizarCliente(index, 'direccion', e.target.value)}
                        />
                      ) : (
                        x.direccion
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.telefono}
                          onChange={(e) => actualizarCliente(index, 'telefono', e.target.value)}
                        />
                      ) : (
                        x.telefono
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.email}
                          onChange={(e) => actualizarCliente(index, 'email', e.target.value)}
                        />
                      ) : (
                        x.email
                      )}
                    </td>
                    <td>
                      {edicionIndex === index ? (
                        <input
                          type="text"
                          value={x.tipoEstado}
                          onChange={(e) => actualizarCliente(index, 'tipoEstado', e.target.value)}
                        />
                      ) : (
                        x.tipoEstado
                      )}
                    </td>
                    <td className="text-center">
                      {edicionIndex === index ? (
                        <button className="btn btn-outline-success" onClick={guardarEdicion}>
                          Guardar
                        </button>
                      ) : (
                        <button className="btn btn-outline-primary" onClick={() => iniciarEdicion(index)}>
                          Editar
                        </button>
                      )}
                    </td>
                    <td>
                      <div style={{ marginTop: 20 }}>
 
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </>
        ) : (
          <p className="h5" style={{ color: 'red' }}>
            No hay registros para listar
          </p>
        )}
      </div>
    </div>
  );
};
 
export default Listar;