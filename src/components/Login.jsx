import React, { useState } from 'react';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Menu } from './Menu';
import { useForm } from 'react-hook-form';
import Logo from './Logo';
 
export const Login = () => {
  const comprobarSesion = () => {
    var sesion = localStorage.getItem('miLogin');
    if (sesion) {
      return JSON.parse(sesion); // true
    } else {
      return false;
    }
  };
 
  const [miLogin, setMiLogin] = useState(comprobarSesion());
 
  // Usar la función useForm para crear el formulario con validación
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onTouched',
  });
 
  function iniciarSesion(data) {
    // Usar los datos del formulario para iniciar sesión
    var txtuser = data.user;
    var txtpass = data.pass;

    if (txtuser.length === 0 || txtpass.length === 0) {
      alert('Complete los datos faltantes!!');
    } else {
      if (txtuser === 'personal' && txtpass === '1234') {
        setMiLogin(true);
        localStorage.setItem('miLogin', true);
        localStorage.setItem('user', txtuser);
        document.getElementById('form_login').style.display = 'none';
      } 
       else {
        setMiLogin(false);
        alert('Error de usuario y/o contraseña!!');
        document.getElementById('txtuser').value = '';
        document.getElementById('txtpass').value = '';
        document.getElementById('txtuser').focus = '';
      }
    }
  }
  return (
    <div className="container" style={{ background: 'lightgray', marginTop: 20, padding: 20 }}>
      {miLogin === false ? (
        <div style={{ display: 'flex' }}>
          <Logo style={{ width: '10%' }} />
          <form id="form_login" onSubmit={handleSubmit(iniciarSesion)} style={{ width: '40%' }}>
            <div>
              <h1 style={{ color: 'blue', textAlign: 'center' }}>LOGIN</h1>
              <label htmlFor="txtuser">
                <strong>Username</strong>
              </label>
              <div style={{ position: 'relative' }}>
                <input
                  type="text"
                  id="txtuser"
                  style={{ textAlign: 'center', paddingLeft: '30px' }}
                  className="form-control"
                  {...register('user', { required: 'El usuario es obligatorio' })}
                />
                <UserOutlined style={{ position: 'absolute', top: '50%', left: '10px', transform: 'translateY(-50%)' }} />
              </div>
              {errors.user && <p className="alerts">{errors.user.message}</p>}
            </div>
            <div>
              <label htmlFor="txtpass">
                <strong>Password</strong>
              </label>
              <div style={{ position: 'relative' }}>
                <input
                  type="password"
                  id="txtpass"
                  style={{ textAlign: 'center', paddingLeft: '30px' }}
                  className="form-control"
                  {...register('pass', {
                    required: 'La contraseña es obligatoria',
                    minLength: { value: 4, message: 'La contraseña debe tener al menos 4 caracteres' },
                  })}
                />
                <LockOutlined style={{ position: 'absolute', top: '50%', left: '10px', transform: 'translateY(-50%)' }} />
              </div>
              {errors.pass && <p className="alerts">{errors.pass.message}</p>}
            </div>
            <br />
            <input type="submit" className="btn btn-primary" value="Login" />
          </form>
        </div>
      ) : (
        <Menu />
      )}
    </div>
  );
};
 
export default Login;