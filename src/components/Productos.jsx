import React, { useState, useEffect } from 'react';

export const Productos = () => {
    const obtenerRegistrosProductos = () => {
        var datos = localStorage.getItem('registrosproductos');
        if (datos !== null) {
            return JSON.parse(datos);
        } else {
            return [];
        }
    };

    const [registrosproductos, setRegistrosProductos] = useState(
        obtenerRegistrosProductos()
    );

    const [nombre, setNombre] = useState('');
    const [marca, setMarca] = useState('');
    const [tipo, setTipo] = useState('');
    const [cantidad, setCantidad] = useState('');
    const [precio, setPrecio] = useState('');

    const botonGuardar = (e) => {
        e.preventDefault();
        var miProducto = { nombre, marca, tipo, cantidad, precio };
        setRegistrosProductos([...registrosproductos, miProducto]);
        limpiarFormulario();
    };

    useEffect(() => {
        localStorage.setItem('registrosproductos', JSON.stringify(registrosproductos));
    }, [registrosproductos]);

    const limpiarFormulario = () => {
        setNombre('');
        setMarca('');
        setTipo('');
        setCantidad('');
        setPrecio('');
        document.getElementById('miFormulario').reset();
    };

    return (
        <div className="bg-light" style={{ marginTop: 20, padding: 20 }}>
            <div className="h3">
                Formulario De Registro De Productos
                <br />
                <form id="miFormulario" onSubmit={botonGuardar}>
                    <div className="row" style={{ marginTop: 20 }}>
                        <div className="col-12">
                            <h4>Productos</h4>
                        </div>
                        <section>
                            <div className="col-6">
                                <input
                                    className="form-control form-control-lg text-center"
                                    type="text"
                                    name="Nombre"
                                    id="Nombre"
                                    placeholder="Ingrese el nombre del producto"
                                    onChange={(e) => setNombre(e.target.value)}
                                    required
                                />
                                <input
                                    className="form-control form-control-lg text-center"
                                    type="text"
                                    name="Marca"
                                    id="Marca"
                                    placeholder="Ingrese la marca"
                                    onChange={(e) => setMarca(e.target.value)}
                                    required
                                />
                                <input
                                    className="form-control form-control-lg text-center"
                                    type="text"
                                    name="Tipo"
                                    id="Tipo"
                                    placeholder="Ingrese el tipo"
                                    onChange={(e) => setTipo(e.target.value)}
                                    required
                                />
                                <input
                                    className="form-control form-control-lg text-center"
                                    type="number"
                                    name="Cantidad"
                                    id="Cantidad"
                                    placeholder="Ingrese la cantidad"
                                    onChange={(e) => setCantidad(e.target.value)}
                                    required
                                />
                                <input
                                    className="form-control form-control-lg text-center"
                                    type="number"
                                    name="Precio"
                                    id="Precio"
                                    placeholder="Ingrese el precio"
                                    onChange={(e) => setPrecio(e.target.value)}
                                    required
                                />
                            </div>
                        </section>
                        <div className="row" style={{ marginTop: 20 }}>
                            <div className="col">
                                <button className="btn btn-primary btn-lg">Guardar Producto</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Productos;