import React, { useState, useEffect } from 'react'
 
export const Clientes = () => {
 
    const obtenerRegistros = () => {
        var datos = localStorage.getItem("registroslogin");
        if (datos !== "null") {
            return JSON.parse(datos);
        } else {
            return [];
        }
    }
 
 
 
    const [registroslogin, setRegistrosLogin] = useState(obtenerRegistros());
 
    const [cedula, setCedula] = useState("");
    const [nombres, setNombres] = useState("");
    const [apellidos, setApellidos] = useState("");
    const [direccion, setDireccion] = useState("");
    const [telefono, setTelefono] = useState("");
    const [email, setEmail] = useState("");
    const [tipoEstado, setTipoEstado] = useState(""); //Activo-Inactivo
 
 


    const opcionesEstado = ['Activo', 'Inactivo'];
 

    const botonGuardar = (e) => {
        e.preventDefault();
        var miObjeto = { cedula, nombres, apellidos, direccion, telefono, email, tipoEstado }
        setRegistrosLogin([...registroslogin, miObjeto]);
        limpiarFormulario();
    }
 
    useEffect(() => {
        localStorage.setItem("registroslogin", JSON.stringify(registroslogin))
    }, [registroslogin]);
 
 
 
    const limpiarFormulario = () => {
        setCedula("");
        setNombres("");
        setApellidos("");
        setDireccion("");
        setTelefono("");
        setEmail("");
        setTipoEstado("");
        document.getElementById("miFormulario").reset();
    }
 
    return (
        <div className="bg-light" style={{ marginTop: 20, padding: 20 }}>
 
            <div className="h3">
                Formulario De Registro De Clientes
                <br />
                <form id="miFormulario" onSubmit={botonGuardar}>
                    <div className="row" style={{ marginTop: 20 }}>
                        <div className="col-12">
                            <h4>Datos del Cliente</h4>
                        </div>
                        <section>
                            <div className="col-6">
                                <input className='form-control form control-lg text-center' type='text' name="Cédula" id="Cédula" placeholder="Ingrese su cédula" onChange={(e) => setCedula(e.target.value)} required />
                                <input className='form-control form control-lg text-center' type='text' name="Nombres" id="Nombrs" placeholder="Ingrese sus Nombres" onChange={(e) => setNombres(e.target.value)} required />
                                <input className='form-control form control-lg text-center' type='text' name="Apellidos" id="Apellidos" placeholder="Ingrese sus Apellidos" onChange={(e) => setApellidos(e.target.value)} required />
                                <input className='form-control form control-lg text-center' type='text' name="Dirección" id="Dirección" placeholder="Ingrese su Dirección" onChange={(e) => setDireccion(e.target.value)} required />
                                <input className='form-control form control-lg text-center' type='text' name="Teléfono" id="Teléfono" placeholder="Ingrese su Teléfono" onChange={(e) => setTelefono(e.target.value)} required />
                                <input className='form-control form control-lg text-center' type='email' name="Email" id="Email" placeholder="Ingrese su Email" onChange={(e) => setEmail(e.target.value)} required />
                                <select
                                    className='form-control form control-lg text-center'
                                    onChange={(e) => setTipoEstado(e.target.value)}
                                    required
                                >
                                    <option value="" disabled selected>Seleccione un estado</option>
                                    {opcionesEstado.map((opcion, index) => (
                                        <option key={index} value={opcion}>{opcion}</option>
                                    ))}
                                </select>
 

                            </div>
                        </section>
 
                        <div className="row" style={{ marginTop: 20 }}>
                            <div className="col">
                                <button className="btn btn-primary btn-lg">Guardar Datos</button>
                            </div>
                        </div>
                    </div>
 
                </form>
            </div>
 
        </div>
 
    )
}
 
export default Clientes